angular.module('starter.services', [])

.factory('Business', function($http, $rootScope, whoopUrl) {

  function all() {
    return $http.get(whoopUrl + '/businesses?page=' + $rootScope.page + '&schedule=' + $rootScope.schedule
      + '&lat=' + $rootScope.latitude + '&lng=' + $rootScope.longitude + '&distance=' + $rootScope.distance);
  }

  function filter(category) {
    return $http.get(whoopUrl + '/businesses?page=' + $rootScope.page + '&schedule=' + $rootScope.schedule
                              + '&category=' + category + '&lat=' + $rootScope.latitude + '&lng='
                              + $rootScope.longitude + '&distance=' + $rootScope.distance);
  }

  function find(id) {
    return $http.get(whoopUrl + '/businesses/' + id);
  }

  function nearby(distance, lat, lng) {
    return $http.get(whoopUrl + '/nearby_businesses/?distance=' + distance + '&lat=' + lat + '&lng=' + lng);
  }

  return {
    all: all,
    filter: filter,
    find: find,
    nearby: nearby
  }
})

.factory('PushService', function($cordovaPush) {
  function getDeviceId() {
    var pnConfig = {
      badge: true,
      sound: true,
      alert: true
    }

    $cordovaPush.register(pnConfig);
  }

  function registerDevice(deviceId) {
    return $http.post(whoopUrl + '/register_device/' + deviceId);
  }

  function unRegisterDevice(deviceId) {
    return $http.delete(whoopUrl + '/register_device/' + deviceId);
  }

  return {
    getDeviceId: getDeviceId,
    registerDevice: registerDevice,
    unRegisterDevice: unRegisterDevice
  }
})

.factory('MapService', function() {
  function setCurrentPosition() {
    navigator.geolocation.getCurrentPosition(function(pos) {
      var latitude = TEST_LOCATION ? -33.8254202 : pos.coords.latitude;
      var longitude = TEST_LOCATION ? -70.6566069 : pos.coords.longitude;

      localStorage.setItem('latitude', latitude);
      localStorage.setItem('longitude', longitude);
    });
  }

  return {
    setCurrentPosition: setCurrentPosition
  }

})

.factory('ShareService', function($cordovaSocialSharing) {
  function successSharing() {
    console.log('==== SHARED SUCCESSFULLY ====');
  }

  function withProvider(provider, img, link, msg) {
    var message = msg || null;
    var image = img || null;
    var link = link || null;

  switch (provider) {
    case 'facebook':
      $cordovaSocialSharing
        .shareViaFacebook(message, image, link)
        .then(successSharing);
      break;
    case 'twitter':
      $cordovaSocialSharing
        .shareViaTwitter(message, image, link)
        .then(successSharing);
      break;
    case 'instagram':
      $cordovaSocialSharing
        .shareViaInstagram(message, image, link)
        .then(successSharing);
      break;
    default:
      console.log('No default option');
      break;
    }
  }

  return {
    withProvider: withProvider
  }
});

