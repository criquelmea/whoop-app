/**
 * @author: Duy Thanh DAO
 * @email: success.ddt@gmail.com
 */
angular.module('starter.controllers', [])

// Home controller
.controller('HomeCtrl', function($scope, $state, $rootScope, Business, MapService,
                                 $ionicPopover, $ionicSlideBoxDelegate, _) {
  $rootScope.page = 1;
  $rootScope.schedule = $rootScope.schedule || 'day';

  MapService.setCurrentPosition();

  function getBusinesses(loadingMore) {
    Business.all()
      .then(function (res) {
        if(!loadingMore) {
          $scope.businesses = [];
        }

        _.each(res.data.businesses, function (business) {
          $scope.businesses.push(business);
          $scope.total_pages = $scope.total_pages || res.data.total_pages;
          $scope.slides = $scope.slides || res.data.slider_images;
          $ionicSlideBoxDelegate.update();

        });
      });
  }

  //Load initial businesses
  getBusinesses();

  //Pagination button
  $scope.loadMore = function () {
    $rootScope.page += 1;
    getBusinesses(true);
  }

  $scope.timeFilter = function (schedule) {
    $rootScope.schedule =  schedule;
    getBusinesses();
  }

//  $ionicPopover.fromTemplateUrl('templates/popover.html', {
//    scope: $scope
//  }).then(function(popover) {
//    $scope.popover = popover;
//  });
//
//// Triggered on a button click, or some other target
//  $scope.openPopover = function($event) {
//    $scope.popover.show($event);
//  }

  //$scope.setDistance = function (distance) {
  //  localStorage.setItem('distance', distance);
  //  //getBusinesses();
  //  location.href = location.href + '/distance/' + distance;
  //  $scope.popover.hide();
  //}
  //
  //$scope.$on('popover.hidden', function () {
  //  $rootScope.distance = localStorage.distance;
  //  getBusinesses();
  //});

  $scope.logout = function() {
    localStorage.removeItem('access_token');
    $state.go('login');
  }

})

// Category controller
.controller('CategoryCtrl', function($scope, $rootScope, $stateParams, $ionicPopover, Business, _) {
  $rootScope.page = 1;
  $rootScope.schedule = $rootScope.schedule || 'day';
  var category = $stateParams.category;


  function getByCategory(loadingMore) {
    Business.filter(category)
      .then(function (res) {
        if(!loadingMore) {
          $scope.businesses = [];
        }

        _.each(res.data.businesses, function (business) {
          $scope.businesses.push(business);
          $scope.total_pages = $scope.total_pages || res.data.total_pages;
        })
      });
  }

  getByCategory();

  $scope.loadMore = function () {
    $rootScope.page += 1;
    getByCategory(true);
  }

  $scope.timeFilter = function (schedule) {
    console.log(schedule);
    $rootScope.schedule =  schedule;
    getByCategory();
  }

  $scope.setDistance = function (distance) {
    $rootScope.distance = distance;
    getBusinesses();
    $scope.popover.hide();
  }

  $ionicPopover.fromTemplateUrl('templates/popover.html', function (popover) {
    $scope.popover = popover;
  });
})

// Business detail controller
.controller('BusinessCtrl', function($scope, $stateParams, $window, $ionicSlideBoxDelegate,
                                     Business, ShareService, TEST_LOCATION) {
  function navigate() {
    var destination = [$scope.business.latitude, $scope.business.longitude];
    var start = [localStorage.latitude, localStorage.longitude];

    launchnavigator.navigate(destination, start);
  }

  Business.find($stateParams.id)
    .then(function (res) {
      $scope.business = res.data;
      $ionicSlideBoxDelegate.update();
    });

  $scope.navigate = navigate;

  $scope.share = function (provider) {
    ShareService.withProvider(provider);
  }
})

.controller('MapCtrl', function($scope, $stateParams, MapService, Business) {
  var mapMarkers = [];
  $scope.businesses = [];
  $scope.distance = 5;

  function clearMapMarkers() {
    _.each(mapMarkers, function(marker) {
      marker.setMap(null);
    });
  }

  function getNearbyBusinesses() {
    Business.nearby($scope.distance, lat, lng)
      .then(function(res) {
        $scope.businesses = res.data;

        _.each($scope.businesses, function(business) {
          var windowContent = "<a href='#/business/ID' style='font-weight:bold;color:#000'>NAME</a>"
            .replace('ID', business.id)
            .replace('NAME', business.name);

          var infoWindow = new google.maps.InfoWindow({
            content: windowContent
          });

          var marker = new google.maps.Marker({
            position: new google.maps.LatLng(business.latitude, business.longitude),
            title: business.name,
            map: $scope.map
          });

          marker.addListener('click', function() {
            infoWindow.open($scope.map, marker);
          });

          mapMarkers.push(marker);
        });
      });
  }

  MapService.setCurrentPosition();

  var lat = localStorage.latitude;
  var lng = localStorage.longitude;

  var location = new google.maps.LatLng(lat, lng);
  var options = {
    zoom: 10,
    center: location
  }

  $scope.map = new google.maps.Map(document.getElementById('map'), options);
  getNearbyBusinesses();
  clearMapMarkers();

  $scope.setDistance = function(distance) {
    clearMapMarkers();
    $scope.distance = distance;
    getNearbyBusinesses();
  }

})

.controller('AuthCtrl', function($scope, $state, $ionicHistory, $cordovaOauth, PushService, appId, DEV_MODE) {
  // hide back button in next view
  $ionicHistory.nextViewOptions({
    disableBack: true
  });

  if(DEV_MODE) {
    localStorage.setItem('access_token', 'testToken');
  }

  function successLogin(res) {
    localStorage.setItem('access_token', res.access_token);

    //PushService.getDeviceId(pnConfig)
    //  .then(function (deviceToken) {
    //    PushService.registerDevice(deviceToken)
    //      .then(function() {
    //        $state.go('home');
    //      });
    //  });
    $state.go('home');
  }

  $scope.facebookLogin = function() {
    if(!localStorage.getItem('access_token')) {
      $cordovaOauth.facebook(appId, ['public_profile']).then(successLogin, function(error) {
        console.log(error);
      });
    }
    $state.go('home');
  }

});
