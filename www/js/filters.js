angular.module('starter.filters', [])
.filter('capitalize', function() {
  return function(input) {
    return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
  }
})

.filter('shorten', function() {
  return function(text, length) {
    return text.substr(0, length);
  }
})

.filter('thumbImg', function() {
  return function(image) {
    return image.replace('original', 'thumb');
  }
});
