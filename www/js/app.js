// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js

var LOCAL_ENV = false;
var TEST_LOCATION = false;
var WHOOP_URL = LOCAL_ENV ? 'http://localhost:3000' : 'https://appwhoop.herokuapp.com';

angular.module('starter', ['ionic','ionic.service.core',  'ionic.service.analytics', 'ngCordova',
                           'ngCordovaOauth', 'starter.controllers', 'starter.services', 'starter.filters'])

.run(function($ionicPlatform, $ionicAnalytics, $cordovaSplashscreen) {
  $ionicAnalytics.register();

  $ionicPlatform.ready(function() {
    // Hide the splash screen in iOS
    if (navigator && navigator.splashscreen) {
      $cordovaSplashscreen.hide();
    }

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }

    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    if (typeof analytics !== 'undefined') {
      analytics.startTrackerWithId('UA-81299322-1');
      console.log('Google Analytics Setup');
    } else {
      console.log('Google Analytics Unavailable');
    }

    var push = new Ionic.Push({
      "debug": true
    });

    push.register(function(token) {
      alert("Device token:" + token.token);
    });
  });
})

.constant('_', window._)
.constant('DEV_MODE', LOCAL_ENV)
.constant('TEST_LOCATION', TEST_LOCATION)
.constant('whoopUrl', WHOOP_URL)
.constant('appId', '1195827460451105')

.config(function($stateProvider, $urlRouterProvider, $httpProvider, $compileProvider, $ionicConfigProvider) {

  $stateProvider

  .state('home', {
    url: '/home',
    templateUrl: 'templates/home.html',
    controller: 'HomeCtrl',
    onEnter: function($state) {
      if(!localStorage.getItem('access_token')) {
        $state.go('login');
      }
    }
  })

  .state('filter', {
    url: '/filter/:category',
    templateUrl: 'templates/category.html',
    controller: 'CategoryCtrl'
  })

  .state('detail', {
    url: '/business/:id',
    templateUrl: 'templates/detail.html',
    controller: 'BusinessCtrl'
  })

  .state('map', {
    url: '/map',
    templateUrl: 'templates/map.html',
    controller: 'MapCtrl'
  })


  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'AuthCtrl',
    onEnter: function() {
      localStorage.removeItem('access_token');
    }
  })

  .state('register', {
    url: '/register',
    templateUrl: 'templates/register.html',
    controller: 'AuthCtrl'
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/home');

  $httpProvider.defaults.useXDomain = true;
  $httpProvider.defaults.withCredentials = true;
  delete $httpProvider.defaults.headers.common["X-Requested-With"];
  $httpProvider.defaults.headers.common["Accept"] = "application/json";
  $httpProvider.defaults.headers.common["Content-Type"] = "application/json";

  $ionicConfigProvider.tabs.position('bottom');

  //$compileProvider.imgSrcSanitizationWhitelist(/^\s*(http|file|blob|cdvfile):|data:image\//);

})

.factory('urlCheckInterceptor', function() {

  return {
    request: function(req) {
      console.log(location.url);
    }
  }
});



